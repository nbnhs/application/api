package main

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

func createApplicant(first, last string, id int64, invited []string) error {
	// Check to see if applicant invited
	fullName := fmt.Sprintf("%s %s", first, last)
	found := false

	for _, item := range invited {
		if fullName == item {
			found = true
			break
		}
	}

	if !found {
		return errors.New("Applicant not invited")
	}

	// Check to see if account already created

	_, err := GetApplicant(id)
	if err != nil {
		return errors.New("Account already exists")
	}

	return SetApplicant(Applicant{Leadership: "Incomplete", Service: "Incomplete", Signature: "Incomplete", Teacher: "Incomplete", First: first, Last: last, ID: id})
}

func getTeacherID(first, last string) (int64, error) {
	name := fmt.Sprintf("%s %s", first, last)
	faculty, err := GetFaculty(name)
	if err == nil {
		return faculty.ID, nil
	}

	err = SetFaculty(Faculty{ID: -1, Name: name})
	if err != nil {
		return 0, err
	}

	faculty, err = GetFaculty(name)
	if err != nil {
		return 0, err
	}
	return faculty.ID, nil
}

func getApplicantProperties(id int64) (Applicant, error) {
	applicant, err := GetApplicant(id)
	if err != nil {
		return Applicant{}, err
	}

	return applicant, nil
}

func putPathInApplicant(id int64, url, property string) error {
	applicant, err := GetApplicant(id)
	if err != nil {
		return err
	}

	switch property {
	case "leadership":
		applicant.Leadership = url
	case "service":
		applicant.Service = url
	case "teacher":
		applicant.Teacher = url
	case "signature":
		applicant.Signature = url
	default:
		return errors.New("Requested property doesn't exist")
	}

	return SetApplicant(applicant)
}

func addImage(id int64, sender, format string, data []byte) error {
	url := fmt.Sprintf("%d/%s.%s", id, sender, format)
	err := SetData(url, data)
	if err != nil {
		return err
	}

	return putPathInApplicant(id, url, sender)
}

func sendSignupConfirmationEmail(id int64) error {
	applicant, err := GetApplicant(id)
	if err != nil {
		return err
	}

	message := fmt.Sprintf(`
<h1 id="hello">Hello %s,</h1>

<p>You have successfully registered an applicant account with the National Honor Society.</p>

<p>You may fill out your application at https://app.nbnhs.org by logging in with your ID card number.</p>

<p>For your reference, the ID card number that you provided to us was %d.</p>

<p>We strongly recommend completing the application on a computer running Chrome, Firefox, or Safari.</p>

<p>Thank you for your interest in the National Honor Society! We can't wait to get to know you.</p>

<p>Sincerely yours,</p>

<p>Milo Gilad<p>

<p>NHS President</p>

<p>(<strong>Note</strong>: this email does not accept replies. Email <a href="mailto:milo.gilad@mynbps.org">milo.gilad@mynbps.org</a> with any questions.)</p>
	`, applicant.First, applicant.ID)

	return SendEmail(applicant.Email, "Registration with the National Honor Society", message)
}

func sendTeacherInvite(teacherEmail, teacherFirst, teacherLast, studentFirst, studentLast string) error {
	name := fmt.Sprintf("%s %s", teacherFirst, teacherLast)
	studentName := fmt.Sprintf("%s %s", studentFirst, studentLast)

	err := SetFaculty(Faculty{Name: name})
	if err != nil {
		return err
	}

	faculty, err := GetFaculty(name)
	if err != nil {
		return err
	}

	url := fmt.Sprintf("https://faculty.nbnhs.org?token=%d", faculty.ID)

	message := fmt.Sprintf(`
<h1 id="hello">Hello %s,</h1>

<p>Your student %s has invited you to fill out their Teacher Recommendation Form for the North Broward National Honor Society.</p>

<p>Please visit the website at %s once you've filled in the paper copy of the form in order to upload it.</p>

<p>Thank you in advance for helping the NHS make a decision about %s.</p>

<p>Sincerely yours,</p>

<p>Milo Gilad<p>

<p>NHS President</p>

<p>(<strong>Note</strong>: this email does not accept replies. Email <a href="mailto:milo.gilad@mynbps.org">milo.gilad@mynbps.org</a> with any questions.)</p>	
	`, name, studentName, url, studentFirst)

	return SendEmail(teacherEmail, "National Honor Society Teacher Recommendation Request", message)
}

func getCompletedApplications() ([]Applicant, error) {
	applicants, err := GetAllApplicants()
	if err != nil {
		return applicants, err
	}

	a := make([]Applicant, 1)

	for _, applicant := range applicants {
		if applicant.Leadership != "Incomplete" && applicant.Service != "Incomplete" && applicant.Signature != "Incomplete" && applicant.Teacher != "Incomplete" {
			a = append(a, applicant)
		}
	}

	return a, nil
}

func getIncompleteApplications() ([]Applicant, error) {
	applicants, err := GetAllApplicants()
	if err != nil {
		return applicants, err
	}

	a := make([]Applicant, 1)

	for _, applicant := range applicants {
		if applicant.Leadership == "Incomplete" && applicant.Service == "Incomplete" && applicant.Signature == "Incomplete" && applicant.Teacher == "Incomplete" {
			a = append(a, applicant)
		}
	}

	return a, nil
}

// func getApplicantsWithProperties() ([]Applicant, error) {

// }

func getImage(url string) ([]byte, error) {
	return GetData(url)
}

func setApplicantPoints(id int64, points string) error {
	applicant, err := GetApplicant(id)
	if err != nil {
		return err
	}

	applicant.Points = points
	return SetApplicant(applicant)
}

func sendFinalNoticeEmails() error {
	incomplete, err := getIncompleteApplications()
	if err != nil {
		return err
	}

	for _, applicant := range incomplete {
		incompleteItems := make([]string, 1)
		if applicant.Leadership == "Incomplete" {
			incompleteItems = append(incompleteItems, "<li>Leadership</li>")
		}
		if applicant.Service == "Incomplete" {
			incompleteItems = append(incompleteItems, "<li>Service</li>")
		}
		if applicant.Teacher == "Incomplete" {
			incompleteItems = append(incompleteItems, "<li>Teacher recommendation</li>")
		}
		if applicant.Signature == "Incomplete" {
			incompleteItems = append(incompleteItems, "<li>Signature</li>")
		}

		message := fmt.Sprintf(`
<h1 id="hello">Hello %s,</h1>

<p>One or more of the materials required for the National Honor Society to make a decision on your application have not been received.</p>

<p>The following must be submitted prior to 8 AM on Feb. 3rd for your application to be evaluated:</p>

<ul>
%s
</ul>

<p>This is the last notice we'll send regarding incomplete items. It is your responsibility to make sure all items are marked "Pending" before the aforementioned deadline.</p>

<p>Sincerely yours,</p>

<p>Milo Gilad<p>

<p>NHS President</p>

<p>(<strong>Note</strong>: this email does not accept replies. Email <a href="mailto:milo.gilad@mynbps.org">milo.gilad@mynbps.org</a> with any questions.)</p>		
		`, applicant.First, strings.Join(incompleteItems, "\n"))

		err = SendEmail(applicant.Email, "Your NHS Application is Incomplete", message)
		if err != nil {
			return err
		}
	}

	return nil
}

func getFinalReportCSV() (string, error) {
	complete, err := getCompletedApplications()
	if err != nil {
		return "", err
	}

	incomplete, err := getIncompleteApplications()
	if err != nil {
		return "", err
	}

	caseName := func(s string) string {
		arr := strings.Split(s, " ")
		result := ""
		for _, elem := range arr {
			result += strings.Title(elem)
		}
		return result
	}
	tallyPoints := func(s string) (int, error) {
		arr := strings.Split(s, ",")
		arr = arr[:len(arr)-1]
		sum := 0
		for _, elem := range arr {
			num, err := strconv.Atoi(elem)
			if err != nil {
				return 0, err
			}
			sum += num
		}
		return sum, nil
	}

	csv := "First Name,Last Name,Status\n"
	for _, applicant := range complete {
		csv += fmt.Sprintf("%s,%s,", caseName(strings.TrimSpace(applicant.First)), caseName(strings.TrimSpace(applicant.Last)))

		points, err := tallyPoints(applicant.Points)
		if err != nil {
			return "", err
		}
		if points >= 7 {
			csv += "Accepted\n"
		} else {
			csv += "Rejected\n"
		}
	}

	for _, applicant := range incomplete {
		csv += fmt.Sprintf("%s,%s,Incomplete", caseName(strings.TrimSpace(applicant.First)), caseName(strings.TrimSpace(applicant.Last)))
	}

	return csv, nil
}
