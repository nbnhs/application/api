package main

import (
	"bytes"
	"context"
	"os"

	"cloud.google.com/go/datastore"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/mailgun/mailgun-go"
)

// Applicant represents a Google Cloud Datastore object with the following properties.
type Applicant struct {
	First      string `json:"first_name"`
	Last       string `json:"last_name"`
	Email      string `json:"email"`
	ID         int64  `json:"id"`
	Leadership string `json:"leadership"`
	Service    string `json:"service"`
	Teacher    string `json:"teacher"`
	Signature  string `json:"signature"`
	Points     string `json:"points"`
}

// Faculty represents a Google Cloud Datastore object with the following properties.
type Faculty struct {
	Name  string `json:"name"`
	Email string `json:"email"`
	ID    int64  `json:"id"`
}

// GetApplicant finds an Applicant with the provided id and returns its representation.
func GetApplicant(id int64) (Applicant, error) {
	a := new(Applicant)

	ctx := context.Background()

	// Creates a client.
	client, err := datastore.NewClient(ctx, os.Getenv("DATASTORE_PROJECT_ID"))
	if err != nil {
		return *a, err
	}
	defer client.Close()

	key := datastore.IDKey("Applicant", id, nil)

	err = client.Get(ctx, key, a)
	if err != nil {
		return *a, err
	}
	return *a, nil
}

// GetAllApplicants returns a representation of all Applicants currently in the Google Cloud Datastore.
func GetAllApplicants() ([]Applicant, error) {
	a := make([]Applicant, 1)

	ctx := context.Background()

	// Creates a client.
	client, err := datastore.NewClient(ctx, os.Getenv("DATASTORE_PROJECT_ID"))
	if err != nil {
		return a, err
	}
	defer client.Close()

	query := datastore.NewQuery("Applicant")

	_, err = client.GetAll(ctx, query, a)
	if err != nil {
		return a, err
	}

	return a, nil
}

// SetApplicant takes a representation of an Applicant and creates its equivalent in Google Cloud Datastore.
func SetApplicant(a Applicant) error {
	ctx := context.Background()

	// Creates a client.
	client, err := datastore.NewClient(ctx, os.Getenv("DATASTORE_PROJECT_ID"))
	if err != nil {
		return err
	}
	defer client.Close()

	key := datastore.IDKey("Applicant", a.ID, nil)

	if _, err := client.Put(ctx, key, &a); err != nil {
		return err
	}

	return nil
}

// GetFaculty finds an Faculty with the provided name and returns its representation.
func GetFaculty(name string) (Faculty, error) {
	f := make([]Faculty, 1)

	ctx := context.Background()

	// Creates a client.
	client, err := datastore.NewClient(ctx, os.Getenv("DATASTORE_PROJECT_ID"))
	if err != nil {
		return f[0], err
	}
	defer client.Close()

	query := datastore.NewQuery("Faculty")
	query.Filter("name = ", name)
	client.GetAll(ctx, query, f)

	return f[0], nil
}

// SetFaculty takes a representation of a Faculty and creates its equivalent in Google Cloud Datastore.
func SetFaculty(f Faculty) error {
	ctx := context.Background()

	// Creates a client.
	client, err := datastore.NewClient(ctx, os.Getenv("DATASTORE_PROJECT_ID"))
	if err != nil {
		return err
	}
	defer client.Close()

	incompleteKeys := []*datastore.Key{datastore.IncompleteKey("Faculty", nil)}
	keys, err := client.AllocateIDs(ctx, incompleteKeys)
	if err != nil {
		return err
	}

	if _, err := client.Put(ctx, keys[0], &f); err != nil {
		return err
	}

	return nil
}

// SetData puts the given data into the given path in an Amazon S3 bucket.
func SetData(path string, data []byte) error {
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-east-1"),
	})

	if err != nil {
		return err
	}

	svc := s3manager.NewUploader(sess)
	dataAsReader := bytes.NewReader(data)

	_, err = svc.Upload(&s3manager.UploadInput{
		Bucket: aws.String("nhs-applications"),
		Key:    aws.String(path),
		Body:   dataAsReader,
	})

	return err
}

// GetData pulls the data at the given path in an Amazon S3 bucket.
func GetData(path string) ([]byte, error) {
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-east-1"),
	})

	if err != nil {
		return nil, err
	}

	svc := s3manager.NewDownloader(sess)
	buf := aws.NewWriteAtBuffer([]byte{})

	_, err = svc.Download(buf, &s3.GetObjectInput{
		Bucket: aws.String("nhs-applications"),
		Key:    aws.String(path),
	})

	if err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

// SendEmail sends an email with Mailgun to the given destination, with the given subject, and containing the given message.
func SendEmail(destination string, subject string, htmlMessage string) error {
	mg := mailgun.NewMailgun("mail.nbnhs.org", os.Getenv("MAILGUN_API_KEY"))

	_, _, err := mg.Send(mg.NewMessage("admissions@mail.nbnhs.org", subject, htmlMessage, destination))
	return err
}
